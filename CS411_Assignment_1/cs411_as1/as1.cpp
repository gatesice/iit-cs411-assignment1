////////////////////////////////////////////////////////////////////////
//
// Assignment 1 support functions (cs411 f13)
//
////////////////////////////////////////////////////////////////////////

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <GL/glut.h>


typedef enum {NOOBJ, RECTOBJ, CIRC, PLINE, PGON} objType;

typedef struct{
  float x,y;      // x and y coordinates of the point
} Point;

typedef struct{
  int   type;     // object type
  float tx,ty;    // translation
  float rotAng;   // rotation
  float sx,sy;    // scale
  float r,g,b;    // color
  float fillFlag; // fill flag
  int   ptsNum;   // number of coordinates in the vertex array
  float param;    // shape parameter (e.g. radius of circle)
  Point pts[100]; // array of vertices
} Obj;


Obj objLst[100];           // object list
int objsNum;               // the total number of objects
float xMin,xMax,yMin,yMax; // specification of the current viewing window


#define NUM_OF_POINTS 100
int readObjLst(char *fn, Obj *objLst)
{
  FILE* fin;
  char c,c1,c2;
  int i;
  Obj *o;

  int objsNum=0;

  // set default values
  Obj curDefault= {NOOBJ, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0, 0.0};

  // open input file
  if(! (fin=fopen(fn,"r")) ){
    printf("\nCan't open file %s\n\7",fn);
    return 0;
  }

  // read objects
  while(fscanf(fin,"%c%c",&c1,&c2)!=EOF) {

    // remove white space
    while(c1== ' ' || c1 =='\n'){
      c1=c2;
      if(fscanf(fin,"%c",&c2)==EOF) return(objsNum);
    }
    switch(c1){

    // add object
    case 'a':
      o = &objLst[objsNum];
      *o = curDefault;

      switch(c2){
      case 'l': // polyline
      case 'p': // polygon
	o->type=PLINE;
	if(c2=='p') o->type=PGON;
	fscanf(fin,"%d",&o->ptsNum);
	for(i=0;i<o->ptsNum;i++)fscanf(fin,"%f%f",&o->pts[i].x,&o->pts[i].y);
	objsNum++; 
	break;
      case 'r': // RECTOBJangle
	o->type=RECTOBJ;
	fscanf(fin,"%f%f%f%f",&o->pts[0].x,&o->pts[0].y,&o->pts[1].x,&o->pts[1].y);
	o->ptsNum=2;
	objsNum++; 
	break;
      case 'c': // circle
	o->type=CIRC;
	fscanf(fin,"%f%f%f",&o->pts[0].x,&o->pts[0].y,&o->param); 
	o->ptsNum=1;
	objsNum++; 
	break;
      default:
	printf("Warning: unknown object type (%c)\n\7",c2);
      }
      break;

    // set property
    case 's':
      o = &curDefault;

      switch(c2){
      case 't': fscanf(fin,"%f%f",&o->tx,&o->ty);       break; // translation
      case 'r': fscanf(fin,"%f",&o->rotAng);            break; // rotation
      case 's': fscanf(fin,"%f%f",&o->sx,&o->sy);       break; // scale
      case 'c': fscanf(fin,"%f%f%f",&o->r,&o->g,&o->b); break; // color
      case 'f': fscanf(fin,"%f",&o->fillFlag);          break; // fill
      default:
	printf("Warning: unknown object property (%c)\n\7",c2);
      }
      break;

    }

    // remove trailing characters to end of line
    while(fread(&c,1,1,fin)) if (c== '\n') break;
  }

  // return number of objects read
  fclose(fin);
  return(objsNum);
}


void processObjs(int objsNum, Obj *objLst)
{
  //
  // YOUR CODE HERE
  // Generate vertices for RECTOBJangles and circles based on their specification
  //

	for (int i = 0; i < objsNum; i ++) {
		Obj &lst = objLst[i];
		switch (lst.type) {
		case objType::RECTOBJ:
			// Rectangle points
			// [0](xLL, yLL), [1](xUR, yUR) >> 
			// [0]', [1]'(xUR, yLL << yUR), [2]' = [1], [3]'(xLL << null, yUR << null)
			lst.pts[2] = lst.pts[1];		// [2]' = [1]
			lst.pts[1].y = lst.pts[0].y;	// [1]'.y = [0].y
			lst.pts[3].x = lst.pts[0].x;	// [3]'.x = [0].x
			lst.pts[3].y = lst.pts[2].y;	// [3]'.y = [1].y = [2]'.y
			lst.ptsNum = 4;
			break;
		case objType::CIRC:
			// Circle points
#define PI 3.1415926535898
			float cx = lst.pts[0].x, cy = lst.pts[0].y;
			for (int i = 0; i < NUM_OF_POINTS; i ++) {
				double angle = 2 * PI * i / NUM_OF_POINTS;
				lst.pts[i].x = cos(angle) * lst.param + cx;
				lst.pts[i].y = sin(angle) * lst.param + cy;
			}
			lst.ptsNum = NUM_OF_POINTS;
#undef PI
			break;
		}
	}
}


void dumpObjLst(int objsNum, Obj *objLst)
{
  int i,j;
  Obj *o;

  // print the opbect type and its points
  for(i=0;i<objsNum;i++){
    o = &objLst[i];

    printf("*** %d:", o->type);
    for(j=0;j<o->ptsNum;j++) printf(" (%3.1f,%3.1f)",o->pts[j].x, o->pts[j].y);
    printf("\n");
  }
}


void reshape(int w, int h)
{
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glOrtho(xMin, xMax, yMin, yMax, -1.0, 1.0);

	glViewport (0, 0, w, h);
}



void init(void) 
{
  glClearColor (0.0, 0.0, 0.0, 0.0);
  glShadeModel (GL_FLAT);

  xMin = yMin = -10.0;
  xMax = yMax =  10.0;
}


void display(void)
{
  int i;
  //
  // YOUR CODE HERE
  // Do some initializations
  //

  glClear (GL_COLOR_BUFFER_BIT);
  glMatrixMode(GL_MODELVIEW);
  
  /* scan the list of objects and draw them */
  for (i=0;i<objsNum;i++) 
  {
    //
    // YOUR CODE HERE
    // Draw the objects (you need approximately 12 lines of code here)
    //

	  if (objType::NOOBJ != objLst[i].type) {		
		glPushMatrix();

		glTranslatef(objLst[i].tx, objLst[i].ty, 0.0);
		glRotatef(objLst[i].rotAng, 0.0, 0.0, 1.0);
		glScalef(objLst[i].sx, objLst[i].sy, 1.0);		

		glColor3f(objLst[i].r, objLst[i].g, objLst[i].b);

		switch (objLst[i].type) {
		case objType::RECTOBJ: case objType::CIRC: case objType::PGON:
			glPolygonMode(GL_FRONT_AND_BACK, 1.0 == objLst[i].fillFlag ? GL_FILL : GL_LINE);
			glBegin(GL_POLYGON); break;
		case objType::PLINE:
			glBegin(GL_LINE_STRIP); break;
		}

		for (int j = 0; j < objLst[i].ptsNum; j ++) {
			glVertex3f(objLst[i].pts[j].x, objLst[i].pts[j].y, 0.0);
		}
		
		glEnd();
		glPopMatrix();

	  }
  }

  glFlush();
}


void help()
{
  printf("Keys:\n");
  printf("-----\n");
  printf("  h   = keys help\n");
  printf("  i/o = zoom in/out\n");
}


void keyboard(unsigned char key, int x, int y)
{
  int i;
  int v[4];
  switch(key){
  case 27: exit(0); /* ESC */
  case 'h': help(); break;

    //
    // YOUR CODE HERE
    // respond to i/o keys (zoom in/out)
    //

  case 'i':
	  xMin = xMin * 0.5; xMax = xMax * 0.5; yMin = yMin * 0.5; yMax = yMax * 0.5;
	  v[4];
	  glGetIntegerv(GL_VIEWPORT, v);
	  reshape(v[2], v[3]);
	  glutPostRedisplay();
	  printf("[INFO]Keypressed 'i' - ZOOM IN\n");
	  break;
  case 'o':
	  xMin = xMin * 2; xMax = xMax * 2; yMin = yMin * 2; yMax = yMax * 2;
	  v[4];
	  glGetIntegerv(GL_VIEWPORT, v);
	  reshape(v[2], v[3]);
	  glutPostRedisplay();
	  printf("[INFO]Keypressed 'o' - ZOOM OUT\n");
	  break;

  }
}



int main(int argc, char *argv[])
{

  // check transferred arguments
  if(argc<2){
    printf("\nUsage draw2d <drawFile>\n\n");
    exit(0);
  }

  // load, dump, and process the object file
  objsNum=readObjLst(argv[1],objLst);
  dumpObjLst(objsNum, objLst);
  processObjs(objsNum, objLst);

  glutInit(&argc, argv);
  glutInitDisplayMode (GLUT_SINGLE | GLUT_RGB);
  glutInitWindowSize (500, 500); 
  glutInitWindowPosition (100, 100);
  glutCreateWindow (argv[0]);
  printf("Using OpenGL version %s\n", glGetString(GL_VERSION));

  init ();
  glutDisplayFunc(display); 
  glutReshapeFunc(reshape);
  glutKeyboardFunc(keyboard);

  glutMainLoop();
  return 0;
}


////////////////////////////////////////////////////////////////////////
// EOF
////////////////////////////////////////////////////////////////////////
